"""Testing pytest fixtures."""
import pytest

TEST_FILE = "00000000-0000-0000-0000-000000000000"
EMPTY_HIERARCHY: dict = dict(
    groups=[], projects=[], subjects=[], sessions=[], acquisitions=[]
)


def test_default_user_core_clients(fw):
    for user in ("admin", "user", "dev"):
        resp = getattr(fw.client, user).get("/users/self")
        assert resp["_id"] == f"{user}@flywheel.test"


def test_get_user_client_w_invalid_api_key(fw):
    with pytest.raises(AttributeError):
        _ = fw.client.foo


def test_default_load_dump(fw):
    data = fw.dump()

    assert data["acquisitions"]
    assert data["apikeys"]
    assert data["devices"]
    assert data["groups"]
    assert data["modalities"]
    assert data["projects"]
    assert data["providers"]
    assert data["roles"]
    assert data["sessions"]
    assert data["singletons"]
    assert data["subjects"]
    assert data["users"]


def test_incremental_loading(fw):
    for _ in range(10):
        fw.load({"projects": [{}]})

    data = fw.dump()
    assert len(data["groups"]) == 2
    assert len(data["projects"]) == 11
    assert len(data["subjects"]) == 1
    assert len(data["sessions"]) == 1
    assert len(data["acquisitions"]) == 1
    assert data["groups"][0]["_id"] == "unknown"
    assert data["groups"][0]["label"] == "Unknown"
    assert data["groups"][1]["_id"] == "group"
    assert data["groups"][1]["label"] == "group"
    assert data["projects"][0]["label"] == "project"
    assert data["projects"][0]["group"] == "group"
    assert data["projects"][0]["parents"] == {"group": "group"}
    assert data["projects"][10]["label"] == "project-10"
    assert data["projects"][10]["group"] == "group"
    assert data["projects"][10]["parents"] == {"group": "group"}


def test_collection_population_w_generator(fw):
    fw.load({"projects": ({} for _ in range(9))})

    data = fw.dump()
    assert len(data["groups"]) == 2
    assert len(data["projects"]) == 10
    assert len(data["subjects"]) == 1
    assert len(data["sessions"]) == 1
    assert len(data["acquisitions"]) == 1
    for i in range(10):
        assert data["projects"][i]["label"] == f"project-{i}" if i else "project"
        assert data["projects"][i]["group"] == "group"
        assert data["projects"][i]["parents"] == {"group": "group"}


def test_creating_complex_hierarchy(fw):
    for _ in range(10):
        fw.load({"subjects": [{}]})
        for _ in range(5):
            fw.load({"sessions": [{}], "acquisitions": [{}, {}]})
            fw.load({"files": [{"uuid": TEST_FILE}]})

    data = fw.dump()
    assert len(data["groups"]) == 2
    assert len(data["projects"]) == 1
    assert len(data["subjects"]) == 11
    assert len(data["sessions"]) == 51
    assert len(data["acquisitions"]) == 101
    assert len(data["files"]) == 50


def test_defaults_reference(fw):
    resp = fw.client.admin.get(f"/projects/{fw.refs.project_id}")
    assert resp.label == "project"


def test_batching(fw, mocker):
    mocker.patch("fw_test_env.database.BATCH_SIZE", 10)

    fw.load({"projects": ({} for _ in range(20))})

    data = fw.dump()
    assert len(data["projects"]) == 21


def test_files(fw):
    parent_ref = {"id": fw.refs.project_id, "type": "project"}
    files = [{"name": "foo", "uuid": TEST_FILE, "parent_ref": parent_ref}]

    fw.load({"files": files})

    resp = fw.client.admin.get(f"/projects/{fw.refs.project_id}/files/foo", raw=True)
    assert resp.content == b"foo bar\n"


@pytest.mark.fw_data(EMPTY_HIERARCHY)
def test_files_missing_parent_ref(fw):
    with pytest.raises(ValueError):
        fw.load({"files": [{"uuid": TEST_FILE}]})


def test_files_missing_id_field_raises(fw):
    with pytest.raises(ValueError):
        fw.load({"files": [{"name": "foo"}]})


def test_files_parent_is_the_last_inserted_container(fw):
    fw.load({"files": [{"uuid": TEST_FILE}]})
    fw.load({"sessions": [{}]})
    fw.load({"files": [{"uuid": TEST_FILE}]})

    data = fw.dump()
    assert data.files[0].parent_ref.type == "acquisition"
    assert data.files[1].parent_ref.type == "session"


@pytest.mark.fw_data(EMPTY_HIERARCHY)
def test_using_custom_default_data(fw):
    data = fw.dump()
    assert len(data["groups"]) == 0
    assert len(data["projects"]) == 0
    assert len(data["subjects"]) == 0
    assert len(data["sessions"]) == 0
    assert len(data["acquisitions"]) == 0
