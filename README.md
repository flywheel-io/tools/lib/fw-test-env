# fw-test-env

`fw-test-env` provides a Flywheel integration test environment that can be used
easily as a Pytest plugin.

It uses docker to spin up

- MongoDB
- Flywheel Core

`fw` fixture exposes the following attributes:

- `fw.load` - Load test data
- `fw.dump` - Dump database
- `fw.reset` - Cleanup database
- `fw.db` - Pymongo database client
- `fw.client` - Get authenticated `CoreClient` instance (e.g. `fw.client.admin`)

When using the `fw` pytest plugin it will load sane defaults into the database.
For more details check the [default fixture yaml](fw_test_env/defaults.yml).

## Installation

Add as a `poetry` dev dependency to your project:

```bash
poetry add --dev fw-test-env
```

## Usage

```python
import pytest

pytest_plugins = "fw_test_env.pytest_plugin"


def test_core_client(fw):
    resp = fw.client.admin.get("/users/self")
    assert resp["_id"] == "admin@flywheel.test"


# skip creating any acquisition
@pytest.mark.fw_data({"acquisitions": []})
def test_with_custom_defaults(fw):
    resp = fw.client.admin.get("/acquisitions")
    assert len(resp) == 0


# specify the filepath relative to "test/data"
# current limitations: test file name needs to be a valid UUID
# and it should start with '0000' (four zero)
def test_with_files(fw):
    test_file = "00000000-0000-0000-0000-000000000000"
    fw.load({"files": [{"name": "test.txt", "uuid": test_file}]})
    acq_id = fw.dump()["acquisitions"][-1]["_id"]
    resp = fw.client.admin.get(f"/acquisitions/{str(acq_id)}/files/test.txt", raw=True)
    assert resp.content == b"foo bar\n"

```

For more examples see: [test_pytest_fixtures.py](tests/test_pytest_fixtures.py)

## Development

Install the project using `poetry` and enable `pre-commit`:

```bash
poetry install
pre-commit install
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
